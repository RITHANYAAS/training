SELECT student.roll_number
      ,student.name AS stud_name
      ,college.name AS college_name
      ,semester_result.grade
      ,semester_result.credits
      ,semester_result.semester
  FROM er.student
      ,er.semester_result 
      ,er.college 
 WHERE student.id=semester_result.stud_id 
   AND college.id = student.college_id
ORDER BY college.name,semester_result.semester;
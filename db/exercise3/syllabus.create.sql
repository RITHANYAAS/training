CREATE TABLE `er`.`syllabus` (
  `id` INT NOT NULL,
  `cdept_id` INT NULL,
  `syllabus_code` CHAR(4) NOT NULL,
  `syllabus_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `cdept_id`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `er`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
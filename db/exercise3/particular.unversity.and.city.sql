SELECT student.roll_number
	  ,student.name AS stud_name
      ,student.gender
      ,student.dob As stud_dob
      ,student.email As stud_email
      ,student.phone As stud_phone
      ,student.address As stud_address
      ,college.name As college_name
      ,department.dept_name
      ,employee.name AS emp_name
  FROM er.department
      ,er.student
	  ,er.employee
      ,er.college_department
      ,er.college
      ,er.university
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id 
   AND employee.college_id = college.id
   AND employee.desig_id = '3'
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'anna university'
   AND college.city = 'Coimbatore';
   
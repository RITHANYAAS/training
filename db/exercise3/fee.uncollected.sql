SELECT university.university_name
      ,college.name AS college_name
      ,semester_fee.semester
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM er.semester_fee
    ,er.university
    ,er.student
    ,er.college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university_name = 'anna university'
 AND paid_year = '2020'
 AND semester = '5'
 AND paid_status = 'notpaid';



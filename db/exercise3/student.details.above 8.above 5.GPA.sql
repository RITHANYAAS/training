SELECT student.id
	  ,student.roll_number
      ,student.name AS STUDENT_NAME
      ,student.gender
      ,college.code AS COLLEGE_CODE
      ,college.name AS COLLEGE_NAME
      ,semester_result.grade
      ,semester_result.credits
  FROM er.university
      ,er.college
      ,er.student
      ,er.semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college_id
   AND semester_result.stud_id = student.id
   AND semester_result.credits > 8
ORDER BY semester_result.credits;
   
SELECT student.id
	  ,student.roll_number
      ,student.name AS STUDENT_NAME
      ,student.gender
      ,college.code AS COLLEGE_CODE
      ,college.name AS COLLEGE_NAME
      ,semester_result.grade
      ,semester_result.credits
  FROM er.university
      ,er.college
      ,er.student
      ,er.semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college_id
   AND semester_result.stud_id = student.id
   AND semester_result.credits > 5
ORDER BY semester_result.credits;
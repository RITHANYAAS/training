CREATE TABLE er.college (id INT AUTO_INCREMENT
             ,code CHAR(4) NOT NULL
             ,name VARCHAR(100) NOT NULL
             ,univ_code CHAR(4) NULL
             ,city VARCHAR(50) NOT NULL
             ,state VARCHAR(50) NOT NULL
             ,year_opened YEAR NOT NULL
             ,PRIMARY KEY (id)
             ,INDEX univ_idx (univ_code ASC) VISIBLE
             ,CONSTRAINT univ
                 FOREIGN KEY (univ_code)
                 REFERENCES er.university(univ_code)
                 ON DELETE NO ACTION
                 ON UPDATE NO ACTION);
SELECT university.`university_name`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM er.semester_fee
    ,er.university
    ,er.student
    ,er.college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university.university_name = 'madras'
 AND paid_status = 'PAID'
 AND paid_year = '2020'
CREATE TABLE `er`.`student` (
  `id` INT NOT NULL,
  `roll_number` CHAR(8) NOT NULL,
  `name` CHAR(8) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `academic_year` YEAR NOT NULL,
  `cdept_id` INT NULL,
  `college_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `collegeid_idx` (`college_id` ASC) VISIBLE,
  INDEX `cdept_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `cdept`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `er`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `collegeid`
    FOREIGN KEY (`college_id`)
    REFERENCES `er`.`college` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
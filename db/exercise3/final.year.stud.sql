SELECT student.roll_number
	  ,student.name AS stud_name
      ,student.gender
      ,student.dob AS stud_dob
      ,student.email AS stud_email
      ,student.phone As stud_phone
      ,student.address AS stud_address
      ,student.academic_year AS 'Batch'
      ,college.name As college_name
      ,department.dept_name
  FROM er.department
      ,er.student
      ,er.college_department
      ,er.college
      ,er.university
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'anna university'
   AND college.city = 'Coimbatore'
   AND student.academic_year = 1999
CREATE TABLE `er`.`professor_syllabus` (
  `emp_id` INT NULL,
  `syllabus_id` INT NULL,
  `semester` TINYINT NOT NULL,
  INDEX `emp_id_idx` (`emp_id` ASC) VISIBLE,
  INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE,
  CONSTRAINT `emp`
    FOREIGN KEY (`emp_id`)
    REFERENCES `er`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `syllabus`
    FOREIGN KEY (`syllabus_id`)
    REFERENCES `er`.`syllabus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
SELECT designation.name 
  ,designation.rank
  ,university.univ_code
  ,college.name AS college_name
  ,department.dept_name
  ,university.university_name
  ,college.city
  ,college.state
  ,college.year_opened
 FROM er.university 
  ,er.college 
  ,er.department 
  ,er.designation 
  ,er.college_department 
  ,er.employee  
 WHERE college.univ_code = university.univ_code 
 AND university.univ_code = department.univ_code 
 AND college_department.udept_code = department.dept_code
 AND employee.cdept_id = college_department.cdept_id 
 AND employee.desig_id = designation.id 
 ORDER BY designation.rank;
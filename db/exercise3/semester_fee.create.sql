CREATE TABLE `er`.`semester_fee` (
  `cdept_id` INT NULL,
  `stud_id` INT NULL,
  `semester` TINYINT NOT NULL,
  `amount` DOUBLE NULL,
  `paid_year` YEAR NULL,
  `paid_status` VARCHAR(10) NOT NULL,
  INDEX `stud_id_idx` (`stud_id` ASC) VISIBLE,
  INDEX `cd_id_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `cd_id`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `er`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `sd_id`
    FOREIGN KEY (`stud_id`)
    REFERENCES `er`.`student` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);
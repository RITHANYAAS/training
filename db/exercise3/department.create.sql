CREATE TABLE `er`.`department` (
  `dept_code` INT NOT NULL,
  `dept_name` VARCHAR(50) NULL,
  `univ_code` CHAR(4) NULL,
  PRIMARY KEY (`dept_code`),
  INDEX `univ_code_idx` (`univ_code` ASC) VISIBLE,
  CONSTRAINT `univ_code`
    FOREIGN KEY (`univ_code`)
    REFERENCES `er`.`university` (`univ_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
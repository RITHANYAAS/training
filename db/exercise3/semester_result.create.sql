CREATE TABLE `er`.`semester_result` (
  `stud_id` INT NULL,
  `syllabus_id` INT NULL,
  `semester` TINYINT NOT NULL,
  `grade` VARCHAR(45) NOT NULL,
  `credits` FLOAT NOT NULL,
  `result_date` DATE NOT NULL,
  INDEX `stud_id_idx` (`stud_id` ASC) VISIBLE,
  INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE,
  CONSTRAINT `stud_id`
    FOREIGN KEY (`stud_id`)
    REFERENCES `er`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `syllabus_id`
    FOREIGN KEY (`syllabus_id`)
    REFERENCES `er`.`syllabus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);                                       


CREATE TABLE `er`.`employee` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `college_id` INT NULL,
  `cdept_id` INT NULL,
  `desig_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `collegeid_idx` (`college_id` ASC) VISIBLE,
  INDEX `cdeptid_idx` (`cdept_id` ASC) VISIBLE,
  INDEX `desigid_idx` (`desig_id` ASC) VISIBLE,
  CONSTRAINT `cid`
    FOREIGN KEY (`college_id`)
    REFERENCES `er`.`college` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cdeptid`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `er`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `desigid`
    FOREIGN KEY (`desig_id`)
    REFERENCES `er`.`designation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

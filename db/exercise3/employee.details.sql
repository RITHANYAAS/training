SELECT employee.id
	  ,employee.name AS emp_name
      ,employee.dob
      ,employee.desig_id
      ,designation.name AS desig_name
      ,designation.rank AS desig_rank
      ,college.name AS college_name
      ,university.univ_code
      ,university.university_name
  FROM er.employee
      ,er.college
      ,er.university
      ,er.designation
 WHERE college.id = employee.college_id
       AND college.univ_code = university.univ_code
       AND designation.id = employee.desig_id
	   AND university.university_name = 'anna university'
ORDER BY employee.id
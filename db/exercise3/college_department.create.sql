CREATE TABLE `er`.`college_department` (
  `cdept_id` INT NOT NULL,
  `udept_code` CHAR(4) NULL,
  `college_id` INT NULL,
  PRIMARY KEY (`cdept_id`),
  INDEX `college_id_idx` (`college_id` ASC) VISIBLE,
  CONSTRAINT `udept_code`
    FOREIGN KEY (`udept_code`)
    REFERENCES `er`.`department` (`univ_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `college_id`
    FOREIGN KEY (`college_id`)
    REFERENCES `er`.`college` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
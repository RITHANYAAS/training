ALTER TABLE `exe1`.`patient` 
ADD INDEX `doctor_id_doctor_idx` (`doctor_id` ASC) VISIBLE;
;
ALTER TABLE `exe1`.`patient` 
ADD CONSTRAINT `doctor_id_doctor`
  FOREIGN KEY (`doctor_id`)
  REFERENCES `exe1`.`doctor` (`doctor_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

SELECT  dob
       ,sysdate()
 FROM   employee 
 Where  date_format(dob,'%d-%m') = date_format(sysdate(),'%d-%m');
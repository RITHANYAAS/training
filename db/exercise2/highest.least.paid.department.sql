SELECT department_id
      ,max(annual_salary) as MaxSalary 
      ,min(annual_salary) as MinSalary
 FROM employee employee 
 group by department_id ;
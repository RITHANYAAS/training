 Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?
/*Requirement:
    To find the class and instance Variables in the given program.
public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
Entities:
    It requires the class called IdentifyMyParts

Function Declaration:
    There is no function is declared in this program.

Jobs To Be Done:
    1)Consider the given class from the question.
    2)Find the class variable(declared with static).
    3)Answer it for the question.
    4)Find the instance Variable.
    5)Answer it for the question.
*/

Class Variable : x
Instance Variable : y
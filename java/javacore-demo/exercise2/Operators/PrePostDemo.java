In the following program, explain why the value "6" is printed twice in a row:
/*
Requirement:
      class PrePostDemo {
    public static void main(String[] args){
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;                     
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6"
        System.out.println(i++);  // "6"
        System.out.println(i);    // "7"
    }
}
Entities:
      PrePostDemo
Function Declaration:
      No function declaration
Jobs to be done:
      In the given program the value "6" is printed twice in a row is explained.
*/
EXPLANATION:
    1) The class PrePostDemo is created.
    2) The variable i is declared and the value as 3.
    3) The value is post incremented.(i=4)
    4) Then the Preincremented value of i is printed(Prints "4"),in this statement at firstb the i value is printed and the increment operation is done(i = 5).
    5) The i value is printed (prints "5").
    6) Then again incremented value of "i" is printed, in this the increment is done and the incremented i value printed (Prints "6")
    7) In next line the Postincremented value of i is printed so that the "i" value printed first and then the increment operation is done.(Prints "6").So that the value "6" is printed two times.
    8) Then the "i" value is printed(prints "7").
Output:
  The code System.out.println(++i); evaluates to 6, ++ evaluates to the incremented value. 
  Next line, System.out.println(i++); evaluates to the current value (6), then increments by one. 
  So "7" doesn't get printed until  next line.

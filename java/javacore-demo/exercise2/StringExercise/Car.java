 How long is the string returned by the following expression? What is the string?
"Was it a car or a cat I saw?".substring(9, 12)
/*Requirement:
    To find how long is the string returned by the following expression? What is the string?
    "Was it a car or a cat I saw?".substring(9, 12)
Entity:
    No Entities declared in this program.

Function declaration:
    No function declared in this program.

Jobs to be done:
    1)Say how many character return.
*/

Solution:
    It return 3 characters in length: car.
/*Requirement:
    To answer the following question.
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.

Entities:
    There is no entity in the given program>
    
Function Declartion:
    There is no function declared in the given program.
    
Jobs to be done:
    1)Find the length of given string.
    2)Find the charater value at  12th positon.
    3)Find the letter "b" at  what postion.

Solution:
    1)32
    2)e
    3)hannah.charAt(15)
/*
Requirement:
  What methods would a class that implements the java.lang.CharSequence interface have to implement?
  
Entity:
  None
  
Function Declaration:
  None
  
*/

Output:
charAt, length, subSequence, and toString.
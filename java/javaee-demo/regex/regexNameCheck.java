/*
Requirement:
    To check the name which starts with M and should be 6 characters.

Entity:
    RegexNameCheck

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Getting the input from the user.
    2)checking the input is string.
    3)Checking the length of the string is 6
    4)Checking the starting letter of the string is M,
    5)If the conditions are true using Regular Expression(REGEX),
     print it is a valid name else print it is not a valid name.
*/
package com.java.training.core.regex;
import java.util.regex.*;
public class regexNameCheck {
	public static void main(String[] args) {
		String name = "mukesh";
		Pattern p = Pattern.compile("[M|m][a-z |A-Z]{5}");
		Matcher m = p.matcher(name);
		if(m.find()) {
			System.out.println(name + " is valid Name");
		}
		else {
			System.out.println(name + " is not valid Name");
		}
	}

}
/*
Requirement:
    demonstrate program explaining basic add and traversal operation of linked hash set

Entity:
    LinkedHashSetDemo
Function Declaration:
    public static void main(String[] args){} 
Jobs To Be Done:
    1)Create a LinkedHashSet
    2)Add elements to the linkedHashSet
    3)create a iterator
    4)iterate the hashmap
        4.1)print the element in the linkedhashset
    
Pseudocode:
    public class LinkedHashSetDemo {
        
        public static void main(String[] args) {
           
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
            add elements to the linkedhashset
            Iterator<String> iterator = new Iterstor<String>();
            while(iterator.hasNext()){
            
                 System.out.Pritln(print the element) ;
            }
        }
    }
*/
package com.java.training.core.setsmap;
import java.util.Iterator;
import java.util.HashSet;
public class LinkedHashSet {

	public static void main(String[] args) {
	    HashSet<String> linkedHashSet = new HashSet<String>();

		linkedHashSet.add("elephant");
		linkedHashSet.add("cat");
		linkedHashSet.add("dog");
		linkedHashSet.add("monkey");
		
		System.out.println(linkedHashSet);

		Iterator<String> iterator = linkedHashSet.iterator();

		while(iterator.hasNext()) {
			
			
			System.out.println(iterator.next());;
		}
	}

}



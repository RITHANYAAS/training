package com.kpr.training.jdbc.exception;
public interface ErrorCode {
	int CODE_001 = 001;
	int CODE_002 = 002;
	ExceptionCode SQLException = null;
	ExceptionCode ID_ZERO = null;
}
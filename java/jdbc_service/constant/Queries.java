package com.kpr.training.jdbc.constant;
public class Queries {
	String createQuery = "INSERT INTO `jdbc_demo`.address (street, city, postal_code) VALUES (?, ?, ?)";
	String deleteQuery = "DELETE from  `jdbc_demo`.address WHERE id=?";
	String readQuery = "SELECT * from `jdbc_demo`.address WHERE `id` = (?)";
	String readAllQuery = "SELECT * from `jdbc_demo`.address";
	String updateQuery = "UPDATE `jdbc_demo`.address SET street = ?, city = ?, postal_code = ? WHERE id = ?";
}